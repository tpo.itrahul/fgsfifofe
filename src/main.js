import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
const axios = require("axios");
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.prototype.$log = console.log.bind(console)

// global variable
//Vue.prototype.$url = 'http://172.21.0.18/TraceMeApi/api/'
Vue.prototype.$url = 'https://localhost:44348/api/'

new Vue({
  el: '#app',
  router,
  store,
  icons,
  template: '<App/>',
  components: {
    App
  }
})
axios.interceptors.request.use(
  config => {
    return {
      ...config,
      headers: {
        Authorization: "api_key",
        'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Access-Control-Allow-Methods': "GET, POST, OPTIONS, PUT, PATCH, DELETE",
            'Access-Control-Allow-Headers': 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization'
      }
    };
  },
  error => Promise.reject(error)
);