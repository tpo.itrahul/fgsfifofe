import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')
const SimpleContainer = () => import('@/containers/SimpleContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// FGS FIFO MASTERS
const AgeingMaster = () => import('@/views/FgsFifo/Master/FGS_Ageing_Master')
const MaterialMaster = () => import('@/views/FgsFifo/Master/FGS_Material_Master')
const BatchTypeMaster = () => import('@/views/FgsFifo/Master/FGS_BatchType_Master')
const CustomerMaster = () => import('@/views/FgsFifo/Master/FGS_Customer_Master')
const CustomerTypeMaster = () => import('@/views/FgsFifo/Master/FGS_Customer_Type_Master')
const LocationMaster = () => import('@/views/FgsFifo/Master/FGS_Location_Master')
const MaterialTypeMaster = () => import('@/views/FgsFifo/Master/FGS_MaterialType_Master')
const SupplierMaster =() => import('@/views/FgsFifo/Master/FGS_Supplier_Master')
const CustomerAgeingMapping =() => import('@/views/FgsFifo/Master/FGS_Customer_Ageing_Mapping')

//FGS FIFO TRANSACTIONS
const DespatchReceiptMob = () => import('@/views/FgsFifo/Transactions/FGS_Despatch_Receipt_Mob')
const DespatchReceipt = () => import('@/views/FgsFifo/Transactions/FGS_Despatch_Receipt')
const DespatchRequest = () => import('@/views/FgsFifo/Transactions/FGS_Despatch_Request')
const MaterialReceiptEntry = () => import('@/views/FgsFifo/Transactions/FGS_Material_Receipt_Entry')

//FGS FIFO REPORTS 
const FGSStockReport = () => import('@/views/FgsFifo/Reports/FGS_Stock_Report')


// TREAD FIFO MASTER
const TraceMaterialMaster = () => import('@/views/TreadFifo/Master/Trace_Material_Master')
const TraceHoldReason = () => import('@/views/TreadFifo/Master/Trace_Hold_Reason')
const TraceMHEMaster = () => import('@/views/TreadFifo/Master/Trace_MHE_Master')
const TracePlantMaster = () => import('@/views/TreadFifo/Master/Trace_Plant_Master')
const TraceStatusMaster = () => import('@/views/TreadFifo/Master/Trace_Status_Master')
const TraceStorageLocationMaster = () => import('@/views/TreadFifo/Master/Trace_Storage_Location_Master')
const TraceUWBAnchorMaster = () => import('@/views/TreadFifo/Master/Trace_UWB_Anchor_Master')
const TraceUWBTagMaster = () => import('@/views/TreadFifo/Master/Trace_UWB_Tag_Master')
const TraceWorkCenterMaster = () => import('@/views/TreadFifo/Master/Trace_WorkCenter_Master')
const TraceWorkCenterTypeMaster = () => import('@/views/TreadFifo/Master/Trace_WorkCenterType_Master')
const TraceUWBTagMHEMapping = () => import('@/views/TreadFifo/Master/Trace_UWB_Tag_MHE_Mapping')
//TREAD FIFO TRANSACTIONS 
const TraceManualEntry = () => import('@/views/TreadFifo/Transactions/Trace_Manual_Entry')
const TraceDeleteEntry = () => import('@/views/TreadFifo/Transactions/Trace_Delete_Entry')
const TraceDisplay = () => import('@/views/TreadFifo/Transactions/Trace_Display')
const TraceHoldEntry = () => import('@/views/TreadFifo/Transactions/Trace_Hold_Entry')
const TraceHoldUpdate = () => import('@/views/TreadFifo/Transactions/Trace_Hold_Update')
const TraceProduction = () => import('@/views/TreadFifo/Transactions/Trace_Production')
const TraceScrapEntry = () => import('@/views/TreadFifo/Transactions/Trace_Scrap_Entry')
const TraceDisplayTB = () => import('@/views/TreadFifo/Transactions/Trace_DisplayTB')
const TraceUpdateOverAged = () => import('@/views/TreadFifo/Transactions/Trace_UpdateLeafTruck')
const TraceUpdateLocation = () => import('@/views/TreadFifo/Transactions/Trace_UpdateLocation')
//TREAD FIFO  REPORTS
 const TraceCurrentStock = () => import('@/views/TreadFifo/Reports/Trace_Current_Stock')
 const TraceEmptyLeafTruck = () => import('@/views/TreadFifo/Reports/Trace_EmptyLeafTruck')
 const TraceHoldReport = () => import('@/views/TreadFifo/Reports/Trace_Hold_Report')
 const TraceProductionReport = () => import('@/views/TreadFifo/Reports/Trace_Production_Report')
 const TraceScrapReport = () => import('@/views/TreadFifo/Reports/Trace_Scrap_Report')
 const TraceTakenTBReport = () => import('@/views/TreadFifo/Reports/Trace_Taken_TB_Report')


// Views - Pages for Login
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')



Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [

    {
      path: '/',
      redirect: '/pages/Login',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'FgsFifo',
          name: 'FgsFifo',
          component: {
            render (c) { return c('router-view') }
          },

          children: [
            {
                name: 'Master',
                path:'Master',
                component: {
                  render (c) { return c('router-view') }
                },
                children: [
                  {
                    name: 'MaterialMaster',
                    component:MaterialMaster,
                    path:'FGS_Material_Master',
                  },
                  {
                    name: 'AgeingMaster',
                    component:AgeingMaster,
                    path:'FGS_Ageing_Master',
                  },
                  {
                    name: 'BatchTypeMaster',
                    component:BatchTypeMaster,
                    path:'FGS_BatchType_Master',
                  },
                  {
                    name: 'CustomerMaster',
                    component:CustomerMaster,
                    path:'FGS_Customer_Master',
                  },
                  {
                    name: 'CustomerTypeMaster',
                    component:CustomerTypeMaster,
                    path:'FGS_Customer_Type_Master',
                  },
                 
                  {
                    name: 'MaterialTypeMaster',
                    component:MaterialTypeMaster,
                    path:'FGS_Material_Type_Master',
                  },
                  {
                    name: 'SupplierMaster',
                    component:SupplierMaster,
                    path:'FGS_Supplier_Master',
                  },

                  {
                    name: 'LocationMaster',
                    component:LocationMaster,
                    path:'FGS_Location_Master',
                  },
                  {
                    name: 'CustomerMaster',
                    component:CustomerMaster,
                    path:'FGS_Customer_Master',
                  },
                  {
                    name: 'CustomerAgeingMapping',
                    component:CustomerAgeingMapping,
                    path:'FGS_Customer_Ageing_Mapping',
                  }

                ]
            },
            {
              name: 'Transactions',
              path:'Transactions',
              component: {
                render (c) { return c('router-view') }
              },
             children: [
              {
                name: 'MaterialReceiptEntry',
                component:MaterialReceiptEntry,
               path:'FGS_Material_Receipt_Entry',
              },
           
       
              {
               name: 'DespatchRequest',
               component:DespatchRequest,
               path:'FGS_Despatch_Request',
              },
              {
                name: 'DespatchReceipt',
                component:DespatchReceipt,
                path:'FGS_Despatch_Receipt',
               },
               {
                name:'DespatchReceiptMob',
                component:DespatchReceiptMob,
                path:'FGS_Despatch_Receipt_Mob',
               },
    
              ]
          }, 
          {
            name: 'Reports',
            path:'Reports',
            component: {
              render (c) { return c('router-view') }
            },
           children: [
            {
              name: 'FGS Stock Report',
              component:FGSStockReport,
              path:'FGS_Stock_Report',
            },
  
            ]
        },
      ]
            },
            //Tread fifo
            
            {
              path: 'TreadFifo',
              name: 'TreadFifo',
              component: {
                render (c) { return c('router-view') }
              },
    
              children: [
                {
                    name: 'Master',
                    path:'Master',
                    component: {
                      render (c) { return c('router-view') }
                    },
                    children: [
                      {
                        name: 'TraceMaterialMaster',
                        component:TraceMaterialMaster,
                        path:'Trace_Material_Master',
                      },
                      {
                        name: 'TraceMHEMaster',
                        component:TraceMHEMaster,
                        path:'Trace_MHE_Master',
                      },
                      {
                        name: 'TracePlantMaster',
                        component:TracePlantMaster,
                        path:'Trace_Plant_Master',
                      },
                      {
                        name: 'TraceStatusMaster',
                        component:TraceStatusMaster,
                        path:'Trace_Status_Master',
                      },
                      {
                        name: 'TraceHoldReason',
                        component:TraceHoldReason,
                        path:'Trace_Hold_Reason',
                      },
                      {
                        name: 'TraceStorageLocationMaster',
                        component:TraceStorageLocationMaster,
                        path:'Trace_Storage_Location_Master',
                      },
                      {
                        name: 'TraceUWBAnchorMaster',
                        component:TraceUWBAnchorMaster,
                        path:'Trace_UWB_Anchor_Master',
                      },
                      {
                        name: 'TraceUWBTagMaster',
                        component:TraceUWBTagMaster,
                        path:'Trace_UWB_Tag_Master',
                      },
                      {
                        name: 'TraceWorkCenterMaster',
                        component:TraceWorkCenterMaster,
                        path:'Trace_WorkCenter_Master',
                      },
                      {
                        name: 'TraceWorkCenterTypeMaster',
                        component:TraceWorkCenterTypeMaster,
                        path:'Trace_WorkCenterType_Master',
                      },
                      {
                        name: 'Trace UWBTag MHE Mapping',
                        component: TraceUWBTagMHEMapping,
                        path: 'Trace_UWB_Tag_MHE_Mapping',
                      },
                    ]
                },
                {
                  name: 'Transactions',
                  path:'Transactions',
                  component: {
                    render (c) { return c('router-view') }
                  },
                 children: [
                  {
                    name: 'Trace Manul Entry',
                    component: TraceManualEntry,
                    path: 'Trace_Manual_Entry',
                  },    
                  {
                    name: 'Trace Delete Entry',
                    component: TraceDeleteEntry,
                    path: 'Trace_Delete_Entry',
                  },
                  {
                    name: 'Trace Display',
                    component: TraceDisplay,
                    path: 'Trace_Display',
                  },
                  {
                    name: 'Trace Hold Entry',
                    component: TraceHoldEntry,
                    path: 'Trace_Hold_Entry',
                  },
                  {
                    name: 'Trace Hold Upadte',
                    component: TraceHoldUpdate,
                    path: 'Trace_Hold_Update',
                  },
                  {
                    name: 'Trace Production',
                    component: TraceProduction,
                    path: 'Trace_Production',
                  },
                  {
                    name: 'TraceScrapEntry',
                    component: TraceScrapEntry,
                    path: 'Trace_Scrap_Entry',
                  },
                  {
                    name: 'Trace TB-Display',
                    component: TraceDisplayTB,
                    path: 'Trace_DisplayTB',
                  },
                  {
                    name: 'Trace Update Over Aged',
                    component: TraceUpdateOverAged,
                    path: 'Trace_UpdateLeafTruck',
                  },
                  
              
                  {
                    name: 'Trace Update Location',
                    component: TraceUpdateLocation,
                    path: 'Trace_UpdateLocation',
                  },
        
                  ]
              }, 
              {
                name: 'Reports',
                path:'Reports',
                component: {
                  render (c) { return c('router-view') }
                },
               children: [
                {
                  name: 'Trace Current Stock',
                component:TraceCurrentStock,
                path:'Trace_Current_Stock',
                },
                {
                  name: 'Trace Empty LeafTruck',
                component:TraceEmptyLeafTruck,
                path:'Trace_EmptyLeafTruck',
                },
                {
                  name: 'Trace Hold Report',
                component:TraceHoldReport,
                path:'Trace_Hold_Report',
                },
                {
                  name: 'Trace Production Report',
                component:TraceProductionReport,
                path:'Trace_Production_Report',
                },
                {
                  name: 'Trace Scrap Report',
                component:TraceScrapReport,
                path:'Trace_Scrap_Report',
                },
                {
                  name: 'Trace Taken TB Report',
                component:TraceTakenTBReport,
                path:'Trace_Taken_TB_Report',
                },
      
                ]
            }, 
              
              
    
                ]
              },
   ]
  },
  {
    path : '/pages',
    redirect :'/pages/Login',
    component: SimpleContainer,
    children: [
     {
       path :'Login',
       name :'Login',
       component :Login
     } 
    ]
  }

 ]
}

