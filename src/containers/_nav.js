export default [
  {
    _name: 'CSidebarNav',
    
    _children: [
        {  
         _name: 'CSidebarNavTitle',
          _children: [''] 
        
        },
        {
      
        _name: 'CSidebarNavDropdown',
        name: 'FGS FIFO',
        route: '/FgsFifo',
        icon: { name: 'cil-layers', 
        class: 'text-white' },
        
         _children: [{
        _name: 'CSidebarNavDropdown',
        name: 'Master',
        route: '/FgsFifo/Master',
        icon: 'cilTask',
        items: [
          {
            name: 'MaterialMaster',
            to: '/FgsFifo/Master/FGS_Material_Master',
            icon:'cil-drop'
          },
          {
            name: 'Material Type Master',
            to: '/FgsFifo/Master/FGS_Material_Type_Master',
            icon:'cil-drop'
          },
          {
            name: 'Supplier Master',
            to: '/FgsFifo/Master/FGS_Supplier_Master',
            icon:'cil-drop'
          },
          {
            name: 'Customer Type Master',
            to: '/FgsFifo/Master/FGS_Customer_Type_Master',
            icon:'cil-drop'
          },
          {
            name: 'Material Ageing Master',
            to: '/FgsFifo/Master/FGS_Ageing_Master',
            icon:'cil-drop'
          },
          {
            name: 'Batch Type Master',
            to: '/FgsFifo/Master/FGS_BatchType_Master',
            icon:'cil-drop'
          },
          {
            name: 'Location Master',
            to: '/FgsFifo/Master/FGS_Location_Master',
            icon:'cil-drop'
          },
          {
            name: 'Customer Master',
            to: '/FgsFifo/Master/FGS_Customer_Master',
            icon:'cil-drop'
          },
          {
            name: 'Customer Ageing Mapping',
            to: '/FgsFifo/Master/FGS_Customer_Ageing_Mapping',
            icon:'cil-drop'
          },
          
         
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Transactions',
        route: '/FgsFifo/Transactions',
        icon: 'cilTask',
        items: [  
          {
            name: 'Material Receipt',
             to: '/FgsFifo/Transactions/FGS_Material_Receipt_Entry',
            icon:'cil-drop'
         },
         {
          name: 'Despatch Request',
          to: '/FgsFifo/Transactions/FGS_Despatch_Request',
           icon:'cil-drop'
         },
        {
          name: 'Process Despatch Request',
          to: '/FgsFifo/Transactions/FGS_Despatch_Receipt',
          icon:'cil-drop'
          },
          // {
          //   name: 'Process Despatch Request Mob',
          //   to: '/Transactions/Fgsfifo/FGS_Despatch_Receipt_Mob',
          //   icon:'cil-drop'
          //   }
         
        ]
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Reports',
        route: '/FgsFifo/Reports',
        icon: 'cilTask',
        items: [  
          {
            name: 'FGS_Stock_Report',
            to: '/FgsFifo/Reports/FGS_Stock_Report',
            icon:'cil-drop'
          },
         
        ]
      }
    ]
  },
  {
  _name: 'CSidebarNavDropdown',
   name: 'TREAD FIFO ',
  route: '/TreadFifo',
  icon: { name: 'cil-layers',
   class: 'text-white' },

    _children:
     [{
      _name: 'CSidebarNavDropdown',
      name: 'Master',
      route: '/TreadFifo/Master',
      icon:'cilTask',
     items: [
      {
        name: 'Trace Plant Master',
        to: '/TreadFifo/Master/Trace_Plant_Master',
        icon:'cil-drop'
      },
      {
        name: 'Trace Work Center Type Master',
        to: '/TreadFifo/Master/Trace_WorkCenterType_Master',
        icon:'cil-drop'
      },
      {
        name: 'Trace Work Center Master',
        to: '/TreadFifo/Master/Trace_WorkCenter_Master',
        icon:'cil-drop'
      },
      {
        name: 'Trace Storage Location Master',
        to: '/TreadFifo/Master/Trace_Storage_Location_Master',
        icon:'cil-drop'
      },
    
      {
        name: 'Trace MHE Master',
        to: '/TreadFifo/Master/Trace_MHE_Master',
        icon:'cil-drop'
      },
      {
        name: 'Trace Material Master',
        to: '/TreadFifo/Master/Trace_Material_Master',
        icon:'cil-drop'
      },
      {
        name: 'Trace UWB Tag Master',
        to: '/TreadFifo/Master/Trace_UWB_Tag_Master',
        icon:'cil-drop'
      },  
      // {
      //   name: 'Trace Status Master',
      //   to: '/TreadFifo/Master/Trace_Status_Master',
      //   icon:'cil-drop'
      // },
      {
        name: 'Trace Hold Reason',
        to: '/TreadFifo/Master/Trace_Hold_Reason',
        icon:'cil-drop'
      },
      {
        name: 'UWB Tag MHE Mapping',
        to: '/TreadFifo/Master/Trace_UWB_Tag_MHE_Mapping',
        icon: 'cil-drop'
      },
      // {
      //   name: 'Anchor WorkCenter Mapping',
      //   to: '/TreadFifo/Master/Trace_Anchor_WC_Mapping',
      //   icon: 'cil-drop'
      // },
      // {
      //   name: 'Trace UWB Anchor Master',
      //   to: '/TreadFifo/Master/Trace_UWB_Anchor_Master',
      //   icon:'cil-drop'
      // },
        
   

      ] 
     },
     {
     _name: 'CSidebarNavDropdown',
      name: 'Transactions',
      route: '/TreadFifo/Transactions',
      icon:'cilTask',
     items: [
      {
        name: 'Production Tag Pinting',
        to: '/TreadFifo/Transactions/Trace_Production',
        icon: 'cil-drop'
      },
      {
        name: 'Manual Tag Printing ',
        to: '/TreadFifo/Transactions/Trace_Manual_Entry',
        icon: 'cil-drop'
      },
          
      {
        name: 'Display Tread ',
        to: '/TreadFifo/Transactions/Trace_Display',
        icon: 'cil-drop'
      },
      {
        name: 'Hold Entry',
        to: '/TreadFifo/Transactions/Trace_Hold_Entry',
        icon: 'cil-drop'
      },
      {
        name: 'Hold Release',
        to: '/TreadFifo/Transactions/Trace_Hold_Update',
        icon: 'cil-drop'
      },
     
      {
        name: 'Scrap Entry',
        to: '/TreadFifo/Transactions/Trace_Scrap_Entry',
        icon: 'cil-drop'
      },     
      // {
      //   name: 'Display-Tyre Building',
      //   to: '/TreadFifo/Transactions/Trace_DisplayTB',
      //   icon: 'cil-drop'
      // }, 
      {
        name: 'Over Aged',
        to: '/TreadFifo/Transactions/Trace_UpdateLeafTruck',
        icon:'cil-drop'
      },
      {
        name: 'Update Location',
        to: '/TreadFifo/Transactions/Trace_UpdateLocation',
        icon:'cil-drop'
      },
      


      ] 
     },
     {
         _name: 'CSidebarNavDropdown',
         name: 'Reports',
         route: '/TreadFifo/Reports',
         icon:'cilTask',
        items: [
          // {
          //   name: 'Current Stock Report',
          //   to: '/TreadFifo/Reports/Trace_Current_Stock',
          //   icon:'cil-drop'
          // },
          {
            name: 'Empty Leaf Truck',
            to: '/TreadFifo/Reports/Trace_EmptyLeafTruck.vue',
            icon:'cil-drop'
          },
          {
            name: 'Hold Report',
            to: '/TreadFifo/Reports/Trace_Hold_Report',
            icon:'cil-drop'
          },
          {
            name: 'Production Report',
            to: '/TreadFifo/Reports/Trace_Production_Report',
            icon:'cil-drop'
          },
          // {
          //   name: 'Scrap Report',
          //   to: '/TreadFifo/Reports/Trace_Scrap_Report',
          //   icon:'cil-drop'
          // },
          {
            name: 'Taken To TB Report',
            to: '/TreadFifo/Reports/Trace_Taken_TB_Report',
            icon:'cil-drop'
          },
        
        ] 
     },
          ],            
          },     

          ]
         },
        ]
  
